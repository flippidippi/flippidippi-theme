# flippidippi theme
Theme for [flippidippi](https://flippidippi.com), but you can too

## 🎨 Color Themes
Dark and light themes based on [gruvbox](https://github.com/morhetz/gruvbox)
- flippidippi dark
- flippidippi light

## 🌿 Product Icons
Emoji based product icons

## 📃 File Icons
Emoji based file icons

---

![https://i.imgur.com/iYrAtHy.png](https://i.imgur.com/iYrAtHy.png)
